#include "Keyboard.h"

Keyboard::Keyboard(int n)
{
    Params.reserve(100);
    id = n;
    idstr = "Keyboard " + ofToString(id);
    type = "Keyboard";

    lastONflag = false;

    Param p;
    p.keyflag = false;
    p.TrigORHold = false;
    p.digitalConnect = false;
    p.digitalValue = false;
    p.keys.push_back("SPACE");
    p.keys.push_back("LEFT");
    p.keys.push_back("RIGHT");
    Params.push_back(p);

    int index = (Params.size() - 1);
    string indexstr = ofToString(index);
    string addrstr = "," + idstr + "," + indexstr;
    Params[0].digitalConnectId="Digital Output Connect"+addrstr;
    //Params[0].analogConnectId="Analog Output Connect"+addrstr;
    gui = new ofxUISuperCanvas(idstr, 300, 300+n*50, 200, 200, OFX_UI_FONT_MEDIUM);
    gui->addSpacer(210,2);
    gui->addRadio("Keys"+addrstr,Params[index].keys,OFX_UI_ORIENTATION_HORIZONTAL);
    gui->addSpacer(0,5);
    gui->addLabelToggle("Trigger/Hold"+addrstr,&Params[index].TrigORHold,false)->setLabelText("Trigger/Hold");
    gui->addSpacer(0,5);
    gui->addSlider("Trig Time"+addrstr,10,1000,&Params[index].trigtime);
    gui->addSpacer(0,5);
    gui->addImageToggle("Digital Output Connect"+addrstr,"Preview.png",&Params[index].digitalConnect)->setColorBack(ofColor(255,50));
    gui->addLabelToggle("Keyboard ON/OFF"+addrstr,&Params[index].digitalValue,false)->setLabelText("Keyboard ON/OFF");
    gui->addWidgetEastOf(gui->getWidget("Keyboard ON/OFF"+addrstr),"Digital Output Connect"+addrstr,true);

    gui->addSpacer(0,5);
    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");

    gui->getWidget("Trigger/Hold"+addrstr)->setDrawOutline(true);
    gui->getWidget("Trig Time"+addrstr)->setDrawOutline(true);
    gui->getWidget("Keyboard ON/OFF"+addrstr)->setDrawOutline(true);
    gui->getWidget("+" + idstr)->setDrawOutline(true);
    gui->autoSizeToFitWidgets();

    gui->setColorBack(ofColor(0,180));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);

    gui->setVisible(false);


}

void Keyboard::addParams()
{

    Param p;
    p.keyflag = false;
    p.TrigORHold = false;
    p.digitalConnect = false;
    p.digitalValue = false;
    p.keys.push_back("SPACE");
    p.keys.push_back("LEFT");
    p.keys.push_back("RIGHT");
    Params.push_back(p);

    int index;
    string indexstr;
    string addrstr;

    //the normal adding doesn't work out, so we reset the whole UI !
    //first get the current position so that we can reset it at the same place...
    int x = gui->getRect()->x;
    int y = gui->getRect()->y;
    //then disable the old UI
    gui->disable();
    //reset it at the current position to hide this resetting from the user
    gui = new ofxUISuperCanvas(idstr, x, y, 200, 200, OFX_UI_FONT_MEDIUM);


    for(int i=0; i<Params.size(); i++)
    {
        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;
        Params[i].digitalConnectId="Digital Output Connect"+addrstr;
        //Params[i].analogConnectId="Analog Output Connect"+addrstr;
        gui->addSpacer(210,2);
        gui->addRadio("Keys"+addrstr,Params[index].keys,OFX_UI_ORIENTATION_HORIZONTAL);
        gui->addSpacer(0,5);
        gui->addLabelToggle("Trigger/Hold"+addrstr,&Params[index].TrigORHold,false)->setLabelText("Trigger/Hold");
        gui->addSpacer(0,5);
        gui->addSlider("Trig Time"+addrstr,10,1000,&Params[index].trigtime);
        gui->addSpacer(0,5);
        gui->addImageToggle("Digital Output Connect"+addrstr,"Preview.png",&Params[index].digitalConnect)->setColorBack(ofColor(255,50));
        gui->addLabelToggle("Keyboard ON/OFF"+addrstr,&Params[index].digitalValue,false)->setLabelText("Keyboard ON/OFF");
        gui->addWidgetEastOf(gui->getWidget("Keyboard ON/OFF"+addrstr),"Digital Output Connect"+addrstr,true);

        gui->getWidget("Trigger/Hold"+addrstr)->setDrawOutline(true);
        gui->getWidget("Trig Time"+addrstr)->setDrawOutline(true);
        gui->getWidget("Keyboard ON/OFF"+addrstr)->setDrawOutline(true);

        gui->addSpacer(0,5);

    }

    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");
    gui->getWidget("+" + idstr)->setDrawOutline(true);

    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(0,180));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);



}
void Keyboard::removeParams()
{
    if(Params.size()>1){
    Params.erase(Params.end()-1);
    int index;
    string indexstr;
    string addrstr;

    //the normal adding doesn't work out, so we reset the whole UI !
    //first get the current position so that we can reset it at the same place...
    int x = gui->getRect()->x;
    int y = gui->getRect()->y;
    //then disable the old UI
    gui->disable();
    //reset it at the current position to hide this resetting from the user
    gui = new ofxUISuperCanvas(idstr, x, y, 200, 200, OFX_UI_FONT_MEDIUM);


    for(int i=0; i<Params.size(); i++)
    {
        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;
        Params[i].digitalConnectId="Digital Output Connect"+addrstr;
        //Params[i].analogConnectId="Analog Output Connect"+addrstr;
        gui->addSpacer(210,2);
        gui->addRadio("Keys"+addrstr,Params[index].keys,OFX_UI_ORIENTATION_HORIZONTAL);
        gui->addSpacer(0,5);
        gui->addLabelToggle("Trigger/Hold"+addrstr,&Params[index].TrigORHold,false)->setLabelText("Trigger/Hold");
        gui->addSpacer(0,5);
        gui->addSlider("Trig Time"+addrstr,10,1000,&Params[index].trigtime);
        gui->addSpacer(0,5);
        gui->addImageToggle("Digital Output Connect"+addrstr,"Preview.png",&Params[index].digitalConnect)->setColorBack(ofColor(255,50));
        gui->addLabelToggle("Keyboard ON/OFF"+addrstr,&Params[index].digitalValue,false)->setLabelText("Keyboard ON/OFF");
        gui->addWidgetEastOf(gui->getWidget("Keyboard ON/OFF"+addrstr),"Digital Output Connect"+addrstr,true);

        gui->getWidget("Trigger/Hold"+addrstr)->setDrawOutline(true);
        gui->getWidget("Trig Time"+addrstr)->setDrawOutline(true);
        gui->getWidget("Keyboard ON/OFF"+addrstr)->setDrawOutline(true);

        gui->addSpacer(0,5);

    }

    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");
    gui->getWidget("+" + idstr)->setDrawOutline(true);

    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(0,180));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);
    }
}
int Keyboard::attached()
{
    int index;
    string indexstr;
    string addrstr;

    // make the gui visible first
    gui->setVisible(true);

    // setting the intrinsic properties and returning apt results to outValue backend variable
    for(int i=0; i<Params.size(); i++)
    {

        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;

        //first get the current key that is selected!
        int keycode;


        ofxUIRadio *radio = (ofxUIRadio *) gui->getWidget("Keys"+addrstr);
        vector <ofxUIToggle*> toggles = radio->getToggles();

        if(toggles[0]->getValue() == 1)
            keycode = VK_SPACE;

        else if( toggles[1]->getValue() == 1)
            keycode = VK_LEFT;

        else if( toggles[2]->getValue() == 1)
            keycode = VK_RIGHT;



        if(Params[i].digitalValue == true && Params[i].TrigORHold == false)  //true-> trig (rebounces back after a while), false-> hold
        {
            keybd_event(keycode,0,0,0);
        }
        else if(Params[i].digitalValue == false && Params[i].TrigORHold == false)
        {
            keybd_event(keycode,0,KEYEVENTF_KEYUP,0);
        }

        //trigger part
        if( Params[i].TrigORHold == true && Params[i].digitalValue == true && Params[i].keyflag == false)
        {
            Params[i].keytime = ofGetElapsedTimeMillis();
            Params[i].keyflag = true;
            keybd_event(keycode,0,0,0);
        }

        //rebounce after time interval of 20ms
        if( Params[i].TrigORHold == true && Params[i].digitalValue == true && Params[i].keyflag == true && ofGetElapsedTimeMillis()- Params[i].keytime >= Params[i].trigtime)
        {
            //tricky part!
            //the keyflag gets reset when the GUI's ONOFF button goes to a low...makes visual sense !
            //but the keypress is deactivated after 20ms
            keybd_event(keycode,0,KEYEVENTF_KEYUP,0);
            //Keyboard_ONOFF = false;
            //keyflag = false;
        }

        if( Params[i].TrigORHold == true && Params[i].digitalValue == false)
        {
            Params[i].keyflag = false;
        }



        // only one set can be ON at a time, so either choose true logic, else the system will allow only one to be ON

        // check which one is on, set the flag high and also store the id. useful later to supply outValue and also to block other ONs
        if( Params[i].digitalValue == true && lastONflag == false)
        {
            lastON = i;
            lastONflag = true;
        }

        // only if the one which was ON is turned OFF, will the flag be cleared and will allow new ONs
        if( Params[i].digitalValue == false && i == lastON && lastONflag == true )
        {
            lastONflag =false;
        }

        // don't allow any other turn ONs
        if( Params[i].digitalValue == true && i!= lastON && lastONflag == true)
        {
            Params[i].digitalValue = false;
        }


        //calculate the co-ordis
        Params[i].digitalConnectx = gui->getRect()->x + gui->getWidget("Digital Output Connect"+addrstr)->getRect()->x; //output dev- gui cordi + widget cordi !
        Params[i].digitalConnecty = gui->getRect()->y + gui->getWidget("Digital Output Connect"+addrstr)->getRect()->y + gui->getWidget("Digital Output Connect"+addrstr)->getRect()->height/2;


    }

    //currently dummy values...
    return 0;
}


bool Keyboard::detached()
{
     bool flag=false;
    if(Params.size()>1)
    flag=true;
    while(Params.size()>1)
    removeParams();
    gui->setVisible(false);
    return flag;

}










