#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup()
{


	ofSetVerticalSync(true); //doc says that this combination keeps app in control
	ofSetFrameRate(120);     //if vsync is off, app runs very fast...

	ofEnableSmoothing();

	red=0;
    green=150;//200;
    blue=150;//200;
    resolution=30;
    ofSetCircleResolution(resolution); //reqd for the circle slider widget
    ofSetCurveResolution(resolution); //reqd for the connecting curves

    bSendSerialMessage = false; // flag so we setup arduino when its ready
    msgack = true; // setting true initially to send the first message

	//setup the serial
	serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
	serial.setup(0, 115200); //first element in the retrieved list ->fix it fo wifi and otherwise !

    memset(bytesReadString, 0, 19); // assign memory and set values

    setupGUIs();
	setupMenu();

}


void testApp::setupGUIs()
{

    //makes GUIs in/visible
    //inputDevice = 0;
    //outputDevice = 0;
    for(int i=0;i<5;i++)
    {
        inputDevice.push_back('0');
        //inValue.push_back(0);
        outputDevice.push_back('0');
        outValue.push_back(0);
    }
    guiMessage = new ofxUISuperCanvas("Message", 600, 10, 200, 200, OFX_UI_FONT_MEDIUM);
    guiMessage->addSpacer(20);
    guiMessage->addLabel("Loading...");

    for(int i=0;i<4;i++){
    ldr[i] = new LDR(i); //why not give a unique id and store the address details etc in a vector! - umm not of much help here :/
    ofAddListener(ldr[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    for(int i=0;i<4;i++){
    ptm[i] = new Ptm(i);
    ofAddListener(ptm[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }
    for(int i=0;i<4;i++){
    led[i] = new LED(i);
    ofAddListener(led[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    //for(int i=0;i<4;i++){
    kbd = new Keyboard(1);
    ofAddListener(kbd->gui->newGUIEvent,this,&testApp::guiEvent);
    //}

    for(int i=0;i<4;i++){
    buz[i] = new Buzzer(i);
    ofAddListener(buz[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    for(int i=0;i<4;i++){
    sro[i] = new Servo(i);
    ofAddListener(sro[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }
}
//--------------------------
void testApp::setupMenu()
{
	xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
    length = 255-xInit;
	ofSetVerticalSync(true);
	ofEnableSmoothing();
    ofSetWindowTitle("PlayKit");
    drawPadding = false;
    bg=0;
    gui = new ofxUICanvas();
    tempvar=1;

    vector<string> items;
    items.push_back("NEW");
    //items.push_back("OPEN");
    //items.push_back("SAVE");
    items.push_back("HELP");
    items.push_back("EXIT");
    gui->setTheme(OFX_UI_THEME_BARBIE);
    gui->addDropDownList("MENU", items, 200);

    tit.loadImage("emo.jpg");
    tit.resize(170,170);

    chk1=0;
    help=0;
    gui->autoSizeToFitWidgets();
    ofAddListener(gui->newGUIEvent,this,&testApp::guiEvent);

   // red = 255; blue = 255; green = 255;
	//ofBackground(red, green, blue);

    gui->setColorBack(ofColor(255,100));
    gui->setWidgetColor(OFX_UI_WIDGET_COLOR_BACK, ofColor(0,0,0));

    led1 = new LEDhelp();
    ofAddListener(led1->gui->newGUIEvent,this,&testApp::guiEvent);
    ledin1= new LEDinfo();
    ofAddListener(ledin1->gui->newGUIEvent,this,&testApp::guiEvent);

    ldr1 = new LDRhelp();
    ofAddListener(ldr1->gui->newGUIEvent,this,&testApp::guiEvent);
   ldrin1 = new LDRinfo();
    ofAddListener(ldrin1->gui->newGUIEvent,this,&testApp::guiEvent);
    buzz1 = new BUZZERhelp();
    ofAddListener(buzz1->gui->newGUIEvent,this,&testApp::guiEvent);
       buzzin1 = new Buzzerinfo();
    ofAddListener(buzzin1->gui->newGUIEvent,this,&testApp::guiEvent);

      pot1 = new POThelp();
    ofAddListener(pot1->gui->newGUIEvent,this,&testApp::guiEvent);

    title.loadFont("cooperBlack.ttf",35);
	title.setLineHeight(18.0f);
	title.setLetterSpacing(1.037);
   // trans.loadImage("title.png");
    msg_help.loadFont("cooperBlack.ttf",20);
	 msg_help.setLineHeight(18.0f);
	 msg_help.setLetterSpacing(1.037);
}
//--------------------------------------------------------------

void testApp::updateArduino(){

    //this is equivalent to firmata's bsetupArduino...
    if(!bSendSerialMessage) //initializing the communication - when receive the go message
    {
        if( serial.readByte() == 'A' )
            bSendSerialMessage = true;

        else if((ofGetElapsedTimef() - readTime) > 2.0f)
            bSendSerialMessage = true;
    }


    if(bSendSerialMessage)  //this is equivalent to firmata's bsetupArduino...
    {

        //first send then receive...

        // Sending messages for I/O pins...

        if( msgack == true )   //send new only when ackd
        {
            msgack = false;
            //ofLog() << "send " << msgack << endl;

            //if( cycleNumber >= 0 &&cycleNumber <= 3 )
            //{
                // NOW you need to send the output values. For your case, the mapping of output values will already have been
                // taken care of by the  programming logic and the UI module backend. So, just write the values !
                //int qq=3000;
                //while(qq--)
               // cout<<"aaaaaaaaaaaa"<<endl;
                serial.writeByte(240); //start sysex
                //serial.writeByte((char)('O'+cycleNumber)); //LED command -> o/p command (pin num)
                serial.writeByte('I');
                serial.writeByte('I');
                for(int i=0;i<4;i++)
                {
                    //serial.writeByte(outputDevice[i]); //device ID
                    serial.writeByte( outValue[i] & 0b01111111); //LSB
                    serial.writeByte( outValue[i] >> 7  & 0b1111111); //MSB
                   //serial.writeByte(0);
                   //serial.writeByte(0);
                }
                serial.writeByte(247); //end sysex
                readTime = ofGetElapsedTimef();

        }


        // the receive part...

        int nRead  = 0;  // a temp variable to keep count per read
        int bt=18;
        unsigned char bytesReturned[bt]; // temp array to read current cycle values
        memset(bytesReadString, 0, bt+1);  // assign memory and set values
        memset(bytesReturned, 0, bt);    // assign memory and set values

        //get the data packet for every cycle
        //while( (nRead = serial.readBytes( bytesReturned, 6)) > 0 ) //TODO: try using the original array, get rid of memset/cpy
        if( (nRead = serial.readBytes( bytesReturned, bt)) == bt)
            nBytesRead = nRead;
         // nRead = serial.readBytes( bytesReturned, bt);
          //nBytesRead = nRead;
        cout<<"ok "<<nBytesRead<<endl;
        for(int i=0;i<bt;i++)
        cout<<(int)bytesReturned[i]<<endl;

        //else
            //nBytesRead = 0;

        // don't know how the fuck is this thing running by commenting the else part!
        // may be if we don't set it to zero, the value carries on for a few cycles till when the data is missing ! :p

        //ofLog() << "nBytesRead " << nBytesRead <<endl;

        //copy the current data to our string - REDUNDANT, find a more efficient way of doing this !
        memcpy(bytesReadString, bytesReturned, bt);
        //cout<<"ok "<<endl;
        //for(int i=0;i<18;i++)
        //cout<<bytesReadString[i]<<endl;


        //this means that now we have the whole message with us, so start processing... fingers crossed for vsync !
        if( nBytesRead == bt && msgack == false )
        {
            // check the command (pin num) then check for device ID
            // while reading the values, it doesn't matter what device is connected, but while writing it is device specific !
            // so no need to check all these things I gues....still confirm later...

            msgack = true;
            //ofLog() << "receive " << msgack <<endl;

            //if( bytesReadString[1] >= 'I' && bytesReadString[1] <= 'L' )
            for(int i=0;i<4;i++)
            {

                //first check the device ID, then assign values to variables appropriately
                //unsigned char prev=inputDevice[pinNumber];
                unsigned char curr=bytesReadString[1+i*3];
                inputDevice[i]=curr;
                if(curr=='A')
                {
                    lsbi = bytesReadString[2+i*3];
                    msbi = bytesReadString[3+i*3];
                    inValue = (msbi << 7) | lsbi;
                    ldr[i]->attached(inValue);
                }
                else if(curr=='P')
                {
                    lsbi = bytesReadString[2+i*3];
                    msbi = bytesReadString[3+i*3];
                    inValue = (msbi << 7) | lsbi;
                    ptm[i]->attached(inValue);

                }
                else
                {
                    //if(i==0)
                   // cout<<"okokokokokokokokok"<<endl;
                     ldrDetach(i);
                     if(ldr[i]->detached())
                     ofAddListener(ldr[i]->gui->newGUIEvent,this,&testApp::guiEvent);

                     ptmDetach(i);
                     if(ptm[i]->detached())
                     ofAddListener(ptm[i]->gui->newGUIEvent,this,&testApp::guiEvent);
                }
            }


            //if( bytesReadString[1] >= 'O' &&bytesReadString[1] <= 'R')
            for(int i=0;i<4;i++)
            {
                //int pinNumber=bytesReadString[1]-'O';
                //unsigned char prev=inputDevice[pinNumber];
                unsigned char curr=bytesReadString[13+i];
                outputDevice[i]=curr;
                //first check the device ID, then assign values to variables appropriately
                if(curr=='L')
                    outValue[i] = led[i]->attached();
                else if(curr=='B')
                    outValue[i] = buz[i]->attached();
                else if(curr=='S')
                    outValue[i] = sro[i]->attached();
                else if(curr=='K')
                    kbdIsConnected=true;
                    //outValue[i] = kbd->attached();
                else
                {
                         outValue[i] = 0;

                         ledDetach(i);
                         if(led[i]->detached())
                         ofAddListener(led[i]->gui->newGUIEvent,this,&testApp::guiEvent);

                         buzDetach(i);
                         if(buz[i]->detached())
                         ofAddListener(buz[i]->gui->newGUIEvent,this,&testApp::guiEvent);

                         sroDetach(i);
                         if(sro[i]->detached())
                         ofAddListener(sro[i]->gui->newGUIEvent,this,&testApp::guiEvent);

                         //kbdDetach(i);
                         //if(kbd->detached())
                         //ofAddListener(kbd->gui->newGUIEvent,this,&testApp::guiEvent);
                }


            }


        }




    }




}


//--------------------------------------------------------------
void testApp::update()
{

    updateArduino();

    //int xvar=10000;
    //while(xvar--)
    //cout<<ldr[i]->Params[0].digitalConnect<<"................"<<endl;
    //detect which device is attached
    if(bSendSerialMessage)
    {

    //input connection size maybe greater than output connection (when u clickd on input), so always go with output conn size
    if(kbdIsConnected==true)
    kbd->attached();
    else
    {
        kbdDetach(0);
        if(kbd->detached())
        ofAddListener(kbd->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    kbdIsConnected=false;
    for(int i=0; i<digitalOutputConnectionVars.size(); i++)
    {
        if( *digitalOutputConditionVars[i] == true && *digitalInputConditionVars[i] == true)
            *digitalOutputConnectionVars[i] = *digitalInputConnectionVars[i];
            //ofLog()<< "outputconn: " << *digitalOutputConnectionVars[i] << endl;
            //ofLog()<< "inputconn: " << *digitalInputConnectionVars[i] << endl;
            //ofLog()<< analogOutputConnectionVars.size() << endl;

        //else if either is false, means connection broken, remove them from the vector!
    }


    if( analogOutputConnectionVars.size()>0)
    {


    for(int i=0; i< (analogOutputConnectionVars.size())-2; i= i+3) // 2 2 2 2 2 !!!!!
    {

        if( *analogOutputConditionVars[i/3] == true && *analogInputConditionVars[i/3] == true)
        {
            //0-value,1-rangeMin,2-rangeMax
            float inRangemin = *analogInputConnectionVars[i+1];
            float inRangemax = *analogInputConnectionVars[i+2];
            float outRangemin = *analogOutputConnectionVars[i+1];
            float outRangemax = *analogOutputConnectionVars[i+2];

            *analogOutputConnectionVars[i] = ofMap(*analogInputConnectionVars[i], inRangemin, inRangemax, outRangemin, outRangemax);
        }

    }

    }




    } // end of if(bSendSerialMessage) block...

}

//--------------------------------------------------------------


void testApp::draw()
{
    //cout<<"hello"<<endl;
    POINT p;
    GetCursorPos(&p);
    //POINT wp;
    //ofPoint ofAppGlutWindow::getWindowPosition()
    //cout<<p.x<<" "<<p.y<<endl;
	//ofBackgroundGradient(ofColor(red,green,blue,255),ofColor(red+50,green-20,blue+7,230),OF_GRADIENT_CIRCULAR);
	ofBackgroundGradient(ofColor(red,green,blue),ofColor(50,50,50),OF_GRADIENT_CIRCULAR);
	ofPushStyle();
	ofEnableBlendMode(OF_BLENDMODE_ALPHA);

    //if( inputDevice == 0 && outputDevice == 0)
    //connected = false;

    ofSetColor(255);
    ofSetLineWidth(5);

    ofNoFill();
    //if(connected)
        //ofLine(lineInX,lineInY,lineOutX,lineOutY);
         /*if( digitalOutputConnectionCordis.size() > 0 ){
        lineInX = *digitalInputConnectionCordis[0];
        lineInY = *digitalInputConnectionCordis[1];
        lineOutX = *digitalOutputConnectionCordis[0];
        lineOutY = * digitalOutputConnectionCordis[1];
    cout<<digitalInputConnectionCordis[0]<<" "<< digitalInputConnectionCordis[1]<<" "<<&(ldr[i]->Params[0].digitalConnectx)<<" "<<&(ldr[i]->Params[0].digitalConnecty)<<endl;
         }*/
         //cout<<"ok";
    if(digi==1 &&firstclickflag && !secondclickflag && *digitalInputConditionVars[digitalInputConditionVars.size()-1] == true)
    {
        lineInX = *digitalInputConnectionCordis[digitalInputConnectionCordis.size()-2];
        lineInY = *digitalInputConnectionCordis[digitalInputConnectionCordis.size()-1];
        lineOutX = p.x-ofGetWindowPositionX();
        lineOutY = p.y-ofGetWindowPositionY();
        ofBezier(lineInX,lineInY,
                 lineInX+(lineOutX-lineInX)/2,lineInY,
                 lineOutX-(lineOutX-lineInX)/2,lineOutY,
                 lineOutX,lineOutY);

    }
    if(digi==0&&firstclickflag && !secondclickflag && *analogInputConditionVars[analogInputConditionVars.size()-1] == true)
    {
        lineInX = *analogInputConnectionCordis[analogInputConnectionCordis.size()-2];
        lineInY = *analogInputConnectionCordis[analogInputConnectionCordis.size()-1];
        lineOutX = p.x-ofGetWindowPositionX();
        lineOutY = p.y-ofGetWindowPositionY();
        ofBezier(lineInX,lineInY,
                 lineInX+(lineOutX-lineInX)/2,lineInY,
                 lineOutX-(lineOutX-lineInX)/2,lineOutY,
                 lineOutX,lineOutY);

    }
    cout<<digitalInputConnectionCordis.size()<<" "<<digitalInputConnectionVars.size()<<endl;
    if( digitalOutputConnectionCordis.size() > 0 )
    {


    for(int i=0; i<digitalOutputConnectionCordis.size()-1; i+=2)
    {
        if( *digitalOutputConditionVars[i/2] == true && *digitalInputConditionVars[i/2] == true)
        {
            lineInX = *digitalInputConnectionCordis[i];
            lineInY = *digitalInputConnectionCordis[i+1];
            lineOutX = *digitalOutputConnectionCordis[i];
            lineOutY = * digitalOutputConnectionCordis[i+1];
            if(*digitalInputConnectionVars[i/2]==true)
            {
                if(wire_togg[i/2])
                ofSetColor(0,255,128);
                else
                ofSetColor(0,153,76);
                wire_togg[i/2]=(wire_togg[i/2]+1)%2;
            }

            ofBezier(lineInX,lineInY,
                 lineInX+(lineOutX-lineInX)/2,lineInY,
                 lineOutX-(lineOutX-lineInX)/2,lineOutY,
                 lineOutX,lineOutY);
                 ofSetColor(255);
        }

    }

    }

    if( analogOutputConnectionCordis.size()>0 )
    {

    for(int i=0; i<analogOutputConnectionCordis.size()-1; i+=2)
    {
        if( *analogOutputConditionVars[i/2] == true && *analogInputConditionVars[i/2] == true)
        {
            lineInX = *analogInputConnectionCordis[i];
            lineInY = *analogInputConnectionCordis[i+1];
            lineOutX = *analogOutputConnectionCordis[i];
            lineOutY = *analogOutputConnectionCordis[i+1];


            ofBezier(lineInX,lineInY,
                 lineInX+(lineOutX-lineInX)/2,lineInY,
                 lineOutX-(lineOutX-lineInX)/2,lineOutY,
                 lineOutX,lineOutY);
        }

    }



    }



    if (!bSendSerialMessage)
		guiMessage->setVisible(true);
    else
        guiMessage->setVisible(false);

	ofPopStyle();
}
//--------------------------------------------------------------


void testApp::guiEvent(ofxUIEventArgs &e)
{
	string name = e.widget->getName();
	int kind = e.widget->getKind();
	//MENU
	if(name == "HELP")
	 {
	if(tempvar){
        led1->visibility();
        help=1;
        ldr1->visibility();
        buzz1->visibility();
        pot1->visibility();
        tempvar=0;
    }
    else{
    led1->invisible();
    ldr1->invisible();
    buzz1->invisible();
    pot1->invisible();
     ldrin1->invisible();
       ledin1->invisible();
       buzzin1->invisible();
    tempvar=1;
      cout << "It is an LED ! IT glows !" << endl;
    }


}

   else if(name == "LEDBTN" ){
	 //ofxUIImageButton *button = (ofxUIImageButton *) e.widget;
     //cout << "It is an LED ! IT glows !" << endl;
    // led1->info();
    //int qq=2000;
  //  while(qq--)
   // cout<<led1->ledbtn<<endl;
    ldr1->invisible();
    buzz1->invisible();
   pot1->invisible();
    ledin1->visibility();
    cout << "It is an LED ! IT glows !" << endl;

    //ofAddListener(led1->gui->newGUIEvent,this,&testApp::guiEvent);
     chk1=1;
     }
     else if(name == "BACK_LED" ){
         ledin1->invisible();
          ldr1->visibility();
         buzz1->visibility();
         pot1->visibility();

     }
	else if(name == "LDRBTN"){
    ldrin1->visibility();
    cout << "It is an LED ! IT glows !" << endl;
    led1->invisible();
    buzz1->invisible();
    pot1->invisible();
    ldr1->visibility();}


	else if(name == "BACK_LDR"){
         ldrin1->invisible();
          ldr1->visibility();
         buzz1->visibility();
         pot1->visibility();
         led1->visibility();
     }
	else if(name == "BUZZERBTN"){
	 //ofxUIImageButton *button = (ofxUIImageButton *) e.widget;
     //cout << "It is an LED ! IT glows !" << endl;
    // led1->info();
    buzzin1->visibility();
    led1->invisible();
    ldr1->invisible();
    pot1->invisible();


     chk3=1;
	}
	 else if(name == "BACK_BUZZER"){
         buzzin1->invisible();
          ldr1->visibility();
         led1->visibility();
         pot1->visibility();
     }
 else if(name == "LDRBTN"){
	 //ofxUIImageButton *button = (ofxUIImageButton *) e.widget;
     //cout << "It is an LED ! IT glows !" << endl;
    // led1->info();
    ldr1->visibility();
    buzz1->invisible();
     led1->invisible();

   pot1->invisible();
//    pot1->invisible();
//    buzzin1->visibility();

    //ofAddListener(led1->gui->newGUIEvent,this,&testApp::guiEvent);
     chk1=1;
     }
   else if(name == "NEW"){
       led1->invisible();
       ldr1->invisible();
       buzz1->invisible();
       ldrin1->invisible();
       ledin1->invisible();
       buzzin1->invisible();
       pot1->invisible();
       chk1=0;
       chk2=0;
       chk3=0;
       help =0;
       bg=1;

   }
   else if(name =="EXIT")
     ofExit();
	//MENU
	// the listener gets removed since you disable the UI...removing widgets would have been better but somehow doesn't work :/
    // so in short we reassign the listener
	for(int i=0;i<4;i++){
	if(name == "+" + ldr[i]->idstr)
	{
	    ldr[i]->addParams();
	    ofAddListener(ldr[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }
    if(name == "+" + ptm[i]->idstr)
	{
	    ptm[i]->addParams();
	    ofAddListener(ptm[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }
	if(name == "+" + led[i]->idstr)
	{
	    led[i]->addParams();
	    ofAddListener(led[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    if(name == "+" + buz[i]->idstr)
    {
        buz[i]->addParams();
        ofAddListener(buz[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    if(name == "+" + sro[i]->idstr)
    {
        sro[i]->addParams();
        ofAddListener(sro[i]->gui->newGUIEvent,this,&testApp::guiEvent);
    }
	}
    if(name == "+" + kbd->idstr)
    {
        kbd->addParams();
        ofAddListener(kbd->gui->newGUIEvent,this,&testApp::guiEvent);
    }

    if(removeDigitalInputConnect(name));//dissconnecting wire is clicked on digital input connect
    else if(removeDigitalOutputConnect(name));//dissconnecting wire is clicked on digital output connect
    else if(removeAnalogInputConnect(name));//dissconnecting wire is clicked on analog input connect
    else if(removeAnalogOutputConnect(name));//dissconnecting wire is clicked on analog output connect
    else if( firstclickflag == false )// CONNECTIONS: check for the signs whenever someone has clicked the widget!
    {

        firstclickdata = ofSplitString(name,",");

        // what if the click is on something else ??? so ensure you have 3 datasets means it has been clicked on the connect!!!
        if( firstclickdata.size() == 3 && !ofContains(alreadyConnected,name) )
        {
        //check re-click on connected condition!
        //inlt xvar=10000;
        //while(xvar--)
        //cout<<" yayayayayayayaya"<<endl;
        int paramindex = ofToInt(firstclickdata[2]);
        string deviceName = firstclickdata[1];
        string connectionType = firstclickdata[0];
       // int xvar=10000;
        //while(xvar--)
        //cout<<ldr[i]->Params[paramindex].digitalConnect<<"................"<<endl;
        for(int i=0;i<4;i++){
        if( deviceName == ldr[i]->idstr ) // get the device name
        {
            //int qq=2000;
            //while(qq--)
            //cout<<"yqyqyqyqyqyqyqyqy"<<endl;
            if( connectionType == "Digital Input Connect" ) // get connection type
            {
               // xvar=10000;
                //while(xvar--)
               // cout<<ldr[i]->Params[paramindex].digitalConnect<<endl;
                if( ldr[i]->Params[paramindex].digitalConnect == true) //check whether that was clicked and is true!
                {
                    digitalInputConnectionID.push_back(name);   // name

                    bool *digiInconnvar = &ldr[i]->Params[paramindex].digitalValue; //get the REFERENCE to the var
                    digitalInputConnectionVars.push_back(digiInconnvar); // add it to APPROPRIATE vector

                    bool *digiIncondvar = &ldr[i]->Params[paramindex].digitalConnect;
                    digitalInputConditionVars.push_back(digiIncondvar);

                    float *x = &ldr[i]->Params[paramindex].digitalConnectx;
                    float *y = &ldr[i]->Params[paramindex].digitalConnecty;
                    digitalInputConnectionCordis.push_back(x);
                    digitalInputConnectionCordis.push_back(y);
                    wire_togg.push_back(0);
                    firstclickflag = true; //only true for all input devs ! -> only if they satisfy all conditions !!!

                    alreadyConnected.push_back(name); // to enable checking re-clicking of already connected stuff !!
                    ofLog() << "entered correct firstclick " << endl;
                    digi=1;
                }

            }

            else if( connectionType == "Analog Input Connect" )
            {

                if( ldr[i]->Params[paramindex].analogConnect == true)
                {
                    analogInputConnectionID.push_back(name);

                    float *analogInconnvar = &ldr[i]->Params[paramindex].analogValue; //value
                    analogInputConnectionVars.push_back(analogInconnvar);

                    float *analogInrangemin = &ldr[i]->Params[paramindex].rangeMin; //rangeMin
                    analogInputConnectionVars.push_back(analogInrangemin);

                    float *analogInrangemax = &ldr[i]->Params[paramindex].rangeMax; //rangeMax
                    analogInputConnectionVars.push_back(analogInrangemax);

                    bool *analogIncondvar = &ldr[i]->Params[paramindex].analogConnect;
                    analogInputConditionVars.push_back(analogIncondvar);

                    float *x = &ldr[i]->Params[paramindex].analogConnectx;
                    float *y = &ldr[i]->Params[paramindex].analogConnecty;
                    analogInputConnectionCordis.push_back(x);
                    analogInputConnectionCordis.push_back(y);

                    firstclickflag = true; //only true for all input devs !
                    alreadyConnected.push_back(name); // to enable checking re-clicking of already connected stuff !!
                    ofLog() << "entered correct firstclick " << endl;
                    digi=0;
                }


            }


        }

        /////////////////////////////////////////////////////
        else if( deviceName == ptm[i]->idstr ) // get the device name
        {
            if( connectionType == "Digital Input Connect" ) // get connection type
            {
               // xvar=10000;
                //while(xvar--)
               // cout<<ptm[i]->Params[paramindex].digitalConnect<<endl;
                if( ptm[i]->Params[paramindex].digitalConnect == true) //check whether that was clicked and is true!
                {
                    digitalInputConnectionID.push_back(name);   // name

                    bool *digiInconnvar = &ptm[i]->Params[paramindex].digitalValue; //get the REFERENCE to the var
                    digitalInputConnectionVars.push_back(digiInconnvar); // add it to APPROPRIATE vector

                    bool *digiIncondvar = &ptm[i]->Params[paramindex].digitalConnect;
                    digitalInputConditionVars.push_back(digiIncondvar);

                    float *x = &ptm[i]->Params[paramindex].digitalConnectx;
                    float *y = &ptm[i]->Params[paramindex].digitalConnecty;
                    digitalInputConnectionCordis.push_back(x);
                    digitalInputConnectionCordis.push_back(y);
                    wire_togg.push_back(0);
                    firstclickflag = true; //only true for all input devs ! -> only if they satisfy all conditions !!!

                    alreadyConnected.push_back(name); // to enable checking re-clicking of already connected stuff !!
                    ofLog() << "entered correct firstclick " << endl;
                    digi=1;
                }

            }

            else if( connectionType == "Analog Input Connect" )
            {

                if( ptm[i]->Params[paramindex].analogConnect == true)
                {
                    analogInputConnectionID.push_back(name);

                    float *analogInconnvar = &ptm[i]->Params[paramindex].analogValue; //value
                    analogInputConnectionVars.push_back(analogInconnvar);

                    float *analogInrangemin = &ptm[i]->Params[paramindex].rangeMin; //rangeMin
                    analogInputConnectionVars.push_back(analogInrangemin);

                    float *analogInrangemax = &ptm[i]->Params[paramindex].rangeMax; //rangeMax
                    analogInputConnectionVars.push_back(analogInrangemax);

                    bool *analogIncondvar = &ptm[i]->Params[paramindex].analogConnect;
                    analogInputConditionVars.push_back(analogIncondvar);

                    float *x = &ptm[i]->Params[paramindex].analogConnectx;
                    float *y = &ptm[i]->Params[paramindex].analogConnecty;
                    analogInputConnectionCordis.push_back(x);
                    analogInputConnectionCordis.push_back(y);

                    firstclickflag = true; //only true for all input devs !
                    alreadyConnected.push_back(name); // to enable checking re-clicking of already connected stuff !!
                    ofLog() << "entered correct firstclick " << endl;
                    digi=0;
                }


            }


        }
        /////////////////////////////////////////////////////
        //first click has to be an input !!! so if it is not, then reset the clicked object and the flag is not set to true !

        else if( deviceName == led[i]->idstr )
        {
            if( connectionType == "Digital Output Connect")
            {

                if( led[i]->Params[paramindex].digitalConnect == true)
                {
                    led[i]->Params[paramindex].digitalConnect = false;
                    firstclickflag = false;
                    ofLog() << "should make led digi connection false: "<< led[i]->Params[paramindex].digitalConnect << endl;
                }

            }

            else if( connectionType == "Analog Output Connect")
            {

                if( led[i]->Params[paramindex].analogConnect == true)
                {
                   led[i]->Params[paramindex].analogConnect = false;
                   firstclickflag = false;
                   ofLog() << "should make led analog connection false " << endl;
                }
            }



        }

        else if( deviceName == buz[i]->idstr )
        {
            if( connectionType == "Digital Output Connect")
            {

                if( buz[i]->Params[paramindex].digitalConnect == true)
                {
                    buz[i]->Params[paramindex].digitalConnect = false;
                    firstclickflag = false;
                    ofLog() << "should make led digi connection false: "<< led[i]->Params[paramindex].digitalConnect << endl;
                }

            }

            else if( connectionType == "Analog Output Connect")
            {

                if( buz[i]->Params[paramindex].analogConnect == true)
                {
                   buz[i]->Params[paramindex].analogConnect = false;
                   firstclickflag = false;
                   ofLog() << "should make led analog connection false " << endl;
                }
            }

        }

        else if( deviceName == sro[i]->idstr )
        {
            if( connectionType == "Digital Output Connect")
            {

                if( sro[i]->Params[paramindex].digitalConnect == true)
                {
                    sro[i]->Params[paramindex].digitalConnect = false;
                    firstclickflag = false;
                    ofLog() << "should make led digi connection false: "<< led[i]->Params[paramindex].digitalConnect << endl;
                }

            }

            else if( connectionType == "Analog Output Connect")
            {

                if( sro[i]->Params[paramindex].analogConnect == true)
                {
                   sro[i]->Params[paramindex].analogConnect = false;
                   firstclickflag = false;
                   ofLog() << "should make led analog connection false " << endl;
                }
            }

        }


        }
        if(deviceName == kbd->idstr)
        {
            if(connectionType == "Digital Output Connect")
            {
                if( kbd->Params[paramindex].digitalConnect == true)
                {
                    kbd->Params[paramindex].digitalConnect = false;
                    firstclickflag = false;
                    ofLog() << "should make kbd digi connection false: "<< kbd->Params[paramindex].digitalConnect << endl;
                }
            }
        }
        } // like that scan all input i.e. firstclickdata[1] == ldr2->idstr...ldr4, sw1...sw4 etc.
    }

    else if( firstclickflag == true && secondclickflag == false && !ofContains(alreadyConnected,name.substr(name.find(","),name.length()) ) ) // without the else if it enters second one in first go!
    {
        //int qq=5000;
       // while(qq--)
        //cout<<"okkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"<<endl;
        secondclickdata = ofSplitString(name,",");

        if(secondclickdata.size() == 3)
        {

        int paramindex = ofToInt(secondclickdata[2]);
        string deviceName = secondclickdata[1];
        string connectionType = secondclickdata[0];
        string firstconnectionType = firstclickdata[0];


        //second click has to be an output !!! so if it is not, then reset the clicked object and the flag is not set to true !
		for(int i=0;i<4;i++){
        if( deviceName == ldr[i]->idstr ) // get the device name
        {
            if( connectionType == "Digital Input Connect" ) // get connection type
            {

                if( ldr[i]->Params[paramindex].digitalConnect == true) //check whether that was clicked and is true!
                {
                    ldr[i]->Params[paramindex].digitalConnect = false;
                    ofLog() << "entered wrong secondclick " << endl;
                    secondclickflag = false; //only true for all output devs, so input dev will be reset !
                }

            }

            else if( connectionType == "Analog Input Connect" )
            {

                if( ldr[i]->Params[paramindex].analogConnect == true)
                {
                    ldr[i]->Params[paramindex].analogConnect = false;
                    ofLog() << "entered wrong secondclick " << endl;
                    secondclickflag = false; //only true for all output devs, so input dev will be reset !
                }


            }


        }

        ///////////
        else if( deviceName == ptm[i]->idstr ) // get the device name
        {
            if( connectionType == "Digital Input Connect" ) // get connection type
            {

                if( ptm[i]->Params[paramindex].digitalConnect == true) //check whether that was clicked and is true!
                {
                    ptm[i]->Params[paramindex].digitalConnect = false;
                    ofLog() << "entered wrong secondclick " << endl;
                    secondclickflag = false; //only true for all output devs, so input dev will be reset !
                }

            }

            else if( connectionType == "Analog Input Connect" )
            {

                if( ptm[i]->Params[paramindex].analogConnect == true)
                {
                    ptm[i]->Params[paramindex].analogConnect = false;
                    ofLog() << "entered wrong secondclick " << endl;
                    secondclickflag = false; //only true for all output devs, so input dev will be reset !
                }


            }


        }
        //////////
        else if( deviceName == led[i]->idstr )
        {
            if( connectionType == "Digital Output Connect" && firstconnectionType == "Digital Input Connect")
            {

                if( led[i]->Params[paramindex].digitalConnect == true)
                {
                    digitalOutputConnectionID.push_back(name);

                    bool *digiOutconnvar = &led[i]->Params[paramindex].digitalValue;
                    digitalOutputConnectionVars.push_back(digiOutconnvar);

                    bool *digiOutcondvar = &led[i]->Params[paramindex].digitalConnect;
                    digitalOutputConditionVars.push_back(digiOutcondvar);

                    float *x = &led[i]->Params[paramindex].digitalConnectx;
                    float *y = &led[i]->Params[paramindex].digitalConnecty;
                    digitalOutputConnectionCordis.push_back(x);
                    digitalOutputConnectionCordis.push_back(y);

                    secondclickflag = true; // put inside this condition, maybe it will make a difference :-p
                    alreadyConnected.push_back(name.substr(name.find(","),name.length()) ); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }



            }

            else if( connectionType == "Digital Output Connect" && firstconnectionType != "Digital Input Connect")
            {

                if( led[i]->Params[paramindex].digitalConnect == true)
                {
                    led[i]->Params[paramindex].digitalConnect = false;
                    secondclickflag = false; //not connected to correct one!
                    ofLog() << "entered wrong secondclick " << endl;
                }



            }

            else if( connectionType == "Analog Output Connect" && firstconnectionType == "Analog Input Connect")
            {

                if( led[i]->Params[paramindex].analogConnect == true)
                {
                    analogOutputConnectionID.push_back(name);

                    float *analogOutconnvar = &led[i]->Params[paramindex].analogValue; //value
                    analogOutputConnectionVars.push_back(analogOutconnvar);

                    float *analogOutrangemin = &led[i]->Params[paramindex].rangeMin; //rangeMin
                    analogOutputConnectionVars.push_back(analogOutrangemin);

                    float *analogOutrangemax = &led[i]->Params[paramindex].rangeMax; //rangeMax
                    analogOutputConnectionVars.push_back(analogOutrangemax);

                    bool *analogOutcondvar = &led[i]->Params[paramindex].analogConnect;
                    analogOutputConditionVars.push_back(analogOutcondvar);

                    float *x = &led[i]->Params[paramindex].analogConnectx;
                    float *y = &led[i]->Params[paramindex].analogConnecty;
                    analogOutputConnectionCordis.push_back(x);
                    analogOutputConnectionCordis.push_back(y);

                    secondclickflag = true;
                    alreadyConnected.push_back(name.substr(name.find(","),name.length())); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }


            }

            else if( connectionType == "Analog Output Connect" && firstconnectionType != "Analog Input Connect")
            {

                if( led[i]->Params[paramindex].analogConnect == true)
                {
                    led[i]->Params[paramindex].analogConnect = false;
                    secondclickflag = false;
                    ofLog() << "entered wrong secondclick " << endl;
                }


            }



        }

        else if( deviceName == buz[i]->idstr )
        {
            if( connectionType == "Digital Output Connect" && firstconnectionType == "Digital Input Connect")
            {

                if( buz[i]->Params[paramindex].digitalConnect == true)
                {
                    digitalOutputConnectionID.push_back(name);

                    bool *digiOutconnvar = &buz[i]->Params[paramindex].digitalValue;
                    digitalOutputConnectionVars.push_back(digiOutconnvar);

                    bool *digiOutcondvar = &buz[i]->Params[paramindex].digitalConnect;
                    digitalOutputConditionVars.push_back(digiOutcondvar);

                    float *x = &buz[i]->Params[paramindex].digitalConnectx;
                    float *y = &buz[i]->Params[paramindex].digitalConnecty;
                    digitalOutputConnectionCordis.push_back(x);
                    digitalOutputConnectionCordis.push_back(y);

                    secondclickflag = true; // put inside this condition, maybe it will make a difference :-p
                    alreadyConnected.push_back(name.substr(name.find(","),name.length())); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }



            }

            else if( connectionType == "Digital Output Connect" && firstconnectionType != "Digital Input Connect")
            {

                if( buz[i]->Params[paramindex].digitalConnect == true)
                {
                    buz[i]->Params[paramindex].digitalConnect = false;
                    secondclickflag = false; //not connected to correct one!
                    ofLog() << "entered wrong secondclick " << endl;
                }



            }

            else if( connectionType == "Analog Output Connect" && firstconnectionType == "Analog Input Connect")
            {

                if( buz[i]->Params[paramindex].analogConnect == true)
                {
                    analogOutputConnectionID.push_back(name);

                    float *analogOutconnvar = &buz[i]->Params[paramindex].analogValue; //value
                    analogOutputConnectionVars.push_back(analogOutconnvar);

                    float *analogOutrangemin = &buz[i]->Params[paramindex].rangeMin; //rangeMin
                    analogOutputConnectionVars.push_back(analogOutrangemin);

                    float *analogOutrangemax = &buz[i]->Params[paramindex].rangeMax; //rangeMax
                    analogOutputConnectionVars.push_back(analogOutrangemax);

                    bool *analogOutcondvar = &buz[i]->Params[paramindex].analogConnect;
                    analogOutputConditionVars.push_back(analogOutcondvar);

                    float *x = &buz[i]->Params[paramindex].analogConnectx;
                    float *y = &buz[i]->Params[paramindex].analogConnecty;
                    analogOutputConnectionCordis.push_back(x);
                    analogOutputConnectionCordis.push_back(y);

                    secondclickflag = true;
                    alreadyConnected.push_back(name.substr(name.find(","),name.length())); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }


            }

            else if( connectionType == "Analog Output Connect" && firstconnectionType != "Analog Input Connect")
            {

                if( buz[i]->Params[paramindex].analogConnect == true)
                {
                    buz[i]->Params[paramindex].analogConnect = false;
                    secondclickflag = false;
                    ofLog() << "entered wrong secondclick " << endl;
                }


            }



        }


        else if( deviceName == sro[i]->idstr )
        {
            if( connectionType == "Digital Output Connect" && firstconnectionType == "Digital Input Connect")
            {

                if( sro[i]->Params[paramindex].digitalConnect == true)
                {

                    digitalOutputConnectionID.push_back(name);

                    bool *digiOutconnvar = &sro[i]->Params[paramindex].digitalValue;
                    digitalOutputConnectionVars.push_back(digiOutconnvar);

                    bool *digiOutcondvar = &sro[i]->Params[paramindex].digitalConnect;
                    digitalOutputConditionVars.push_back(digiOutcondvar);

                    float *x = &sro[i]->Params[paramindex].digitalConnectx;
                    float *y = &sro[i]->Params[paramindex].digitalConnecty;
                    digitalOutputConnectionCordis.push_back(x);
                    digitalOutputConnectionCordis.push_back(y);

                    secondclickflag = true; // put inside this condition, maybe it will make a difference :-p
                    alreadyConnected.push_back(name.substr(name.find(","),name.length())); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }



            }

            else if( connectionType == "Digital Output Connect" && firstconnectionType != "Digital Input Connect")
            {

                if( sro[i]->Params[paramindex].digitalConnect == true)
                {
                    sro[i]->Params[paramindex].digitalConnect = false;
                    secondclickflag = false; //not connected to correct one!
                    ofLog() << "entered wrong secondclick " << endl;
                }



            }

            else if( connectionType == "Analog Output Connect" && firstconnectionType == "Analog Input Connect")
            {

                if( sro[i]->Params[paramindex].analogConnect == true)
                {
                    analogOutputConnectionID.push_back(name);


                    float *analogOutconnvar = &sro[i]->Params[paramindex].analogValue; //value
                    analogOutputConnectionVars.push_back(analogOutconnvar);

                    float *analogOutrangemin = &sro[i]->Params[paramindex].rangeMin; //rangeMin
                    analogOutputConnectionVars.push_back(analogOutrangemin);

                    float *analogOutrangemax = &sro[i]->Params[paramindex].rangeMax; //rangeMax
                    analogOutputConnectionVars.push_back(analogOutrangemax);

                    bool *analogOutcondvar = &sro[i]->Params[paramindex].analogConnect;
                    analogOutputConditionVars.push_back(analogOutcondvar);

                    float *x = &sro[i]->Params[paramindex].analogConnectx;
                    float *y = &sro[i]->Params[paramindex].analogConnecty;
                    analogOutputConnectionCordis.push_back(x);
                    analogOutputConnectionCordis.push_back(y);

                    secondclickflag = true;
                    alreadyConnected.push_back(name.substr(name.find(","),name.length())); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }


            }

            else if( connectionType == "Analog Output Connect" && firstconnectionType != "Analog Input Connect")
            {

                if( sro[i]->Params[paramindex].analogConnect == true)
                {
                    sro[i]->Params[paramindex].analogConnect = false;
                    secondclickflag = false;
                    ofLog() << "entered wrong secondclick " << endl;
                }


            }



        }






		}////////
        if( deviceName == kbd->idstr )
        {
            if( connectionType == "Digital Output Connect" && firstconnectionType == "Digital Input Connect")
            {

                if( kbd->Params[paramindex].digitalConnect == true)
                {

                    digitalOutputConnectionID.push_back(name);

                    bool *digiOutconnvar = &kbd->Params[paramindex].digitalValue;
                    digitalOutputConnectionVars.push_back(digiOutconnvar);

                    bool *digiOutcondvar = &kbd->Params[paramindex].digitalConnect;
                    digitalOutputConditionVars.push_back(digiOutcondvar);

                    float *x = &kbd->Params[paramindex].digitalConnectx;
                    float *y = &kbd->Params[paramindex].digitalConnecty;
                    digitalOutputConnectionCordis.push_back(x);
                    digitalOutputConnectionCordis.push_back(y);

                    secondclickflag = true; // put inside this condition, maybe it will make a difference :-p
                    alreadyConnected.push_back(name.substr(name.find(","),name.length())); // to enable checking re-clicking of already connected stuff !!
                    ofLog()<< "entered correct second click" << endl;

                }



            }

            else if( connectionType == "Digital Output Connect" && firstconnectionType != "Digital Input Connect")
            {

                if( kbd->Params[paramindex].digitalConnect == true)
                {
                    kbd->Params[paramindex].digitalConnect = false;
                    secondclickflag = false; //not connected to correct one!
                    ofLog() << "entered wrong secondclick " << endl;
                }



            }

        }
        }

    }
    //if click on a connection and if suddenly

    //reset flags to accept new connections !
    if( firstclickflag == true && secondclickflag == true)
    {
        firstclickflag = false;
        secondclickflag = false;
    }

}
//--------------------------------------------------------------

bool testApp::removeDigitalInputConnect(string name)
{
    if(ofContains(digitalInputConnectionID,name))
    {


            int removeInd=find(digitalInputConnectionID.begin(), digitalInputConnectionID.end(),name)-digitalInputConnectionID.begin();
            digitalInputConnectionID.erase(digitalInputConnectionID.begin()+removeInd);
            digitalInputConnectionVars.erase(digitalInputConnectionVars.begin()+removeInd);
            *digitalInputConditionVars[removeInd]=false;
            digitalInputConditionVars.erase(digitalInputConditionVars.begin()+removeInd);
            digitalInputConnectionCordis.erase(digitalInputConnectionCordis.begin()+2*removeInd,digitalInputConnectionCordis.begin()+2*removeInd+2);
            wire_togg.erase(wire_togg.begin()+removeInd);
            int removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),name)-alreadyConnected.begin();
            alreadyConnected.erase(alreadyConnected.begin()+removeIndac);

            //int qq=3000;
            //while(qq--)
            //cout<<"qqqqqqqqqqqqqqqqqqqqqqqqqqqq  "<<firstclickflag <<endl;
            if(firstclickflag == false)    //remove anly if corresponding output vars were pushed
            {
                string outName=digitalOutputConnectionID[removeInd];
                digitalOutputConnectionID.erase(digitalOutputConnectionID.begin()+removeInd);
                digitalOutputConnectionVars.erase(digitalOutputConnectionVars.begin()+removeInd);
                *digitalOutputConditionVars[removeInd]=false;
                digitalOutputConditionVars.erase(digitalOutputConditionVars.begin()+removeInd);
                digitalOutputConnectionCordis.erase(digitalOutputConnectionCordis.begin()+2*removeInd,digitalOutputConnectionCordis.begin()+2*removeInd+2);
                removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),outName.substr(outName.find(","),outName.length()))-alreadyConnected.begin();
                alreadyConnected.erase(alreadyConnected.begin()+removeIndac);
            }
             //cout<<"qqqqqqqqqqqqqqqqqqqqqqqqqqqq  "<<firstclickflag <<endl;
            firstclickflag = false;
             //cout<<"qqqqqqqqqqqqqqqqqqqqqqqqqqqq  "<<firstclickflag <<endl;
    return true;
    }
    return false;
}
bool testApp::removeDigitalOutputConnect(string name)
{
    if(ofContains(digitalOutputConnectionID,name))
    {
        int removeInd=find(digitalOutputConnectionID.begin(), digitalOutputConnectionID.end(),name)-digitalOutputConnectionID.begin();
        digitalOutputConnectionID.erase(digitalOutputConnectionID.begin()+removeInd);
        digitalOutputConnectionVars.erase(digitalOutputConnectionVars.begin()+removeInd);
        *digitalOutputConditionVars[removeInd]=false;
        digitalOutputConditionVars.erase(digitalOutputConditionVars.begin()+removeInd);
        digitalOutputConnectionCordis.erase(digitalOutputConnectionCordis.begin()+2*removeInd,digitalOutputConnectionCordis.begin()+2*removeInd+2);
        wire_togg.erase(wire_togg.begin()+removeInd);
        int removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),name.substr(name.find(","),name.length()))-alreadyConnected.begin();
        alreadyConnected.erase(alreadyConnected.begin()+removeIndac);

        string inName=digitalInputConnectionID[removeInd];
        digitalInputConnectionID.erase(digitalInputConnectionID.begin()+removeInd);
        digitalInputConnectionVars.erase(digitalInputConnectionVars.begin()+removeInd);
        *digitalInputConditionVars[removeInd]=false;
        digitalInputConditionVars.erase(digitalInputConditionVars.begin()+removeInd);
        digitalInputConnectionCordis.erase(digitalInputConnectionCordis.begin()+2*removeInd,digitalInputConnectionCordis.begin()+2*removeInd+2);
        removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),inName)-alreadyConnected.begin();
        alreadyConnected.erase(alreadyConnected.begin()+removeIndac);
        return true;
    }
    return false;
}
bool testApp::removeAnalogInputConnect(string name)
{
    if(ofContains(analogInputConnectionID,name))
    {
       // cout<<""<<endl;

        int removeInd=find(analogInputConnectionID.begin(), analogInputConnectionID.end(),name)-analogInputConnectionID.begin();
        analogInputConnectionID.erase(analogInputConnectionID.begin()+removeInd);
        analogInputConnectionVars.erase(analogInputConnectionVars.begin()+3*removeInd,analogInputConnectionVars.begin()+3*removeInd+3);
        *analogInputConditionVars[removeInd]=false;
        analogInputConditionVars.erase(analogInputConditionVars.begin()+removeInd);
        analogInputConnectionCordis.erase(analogInputConnectionCordis.begin()+2*removeInd,analogInputConnectionCordis.begin()+2*removeInd+2);
        int removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),name)-alreadyConnected.begin();
        alreadyConnected.erase(alreadyConnected.begin()+removeIndac);

        if(firstclickflag == false)
        {

            //remove output params
            //removeInd=find(analogOutputConnectionID.begin(), analogOutputConnectionID.end(),name)-analogOutputConnectionID.begin();
            string outName=analogOutputConnectionID[removeInd];
            analogOutputConnectionID.erase(analogOutputConnectionID.begin()+removeInd);
            analogOutputConnectionVars.erase(analogOutputConnectionVars.begin()+3*removeInd,analogOutputConnectionVars.begin()+3*removeInd+3);
            *analogOutputConditionVars[removeInd]=false;
            analogOutputConditionVars.erase(analogOutputConditionVars.begin()+removeInd);
            analogOutputConnectionCordis.erase(analogOutputConnectionCordis.begin()+2*removeInd,analogOutputConnectionCordis.begin()+2*removeInd+2);
            removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),outName.substr(outName.find(","),outName.length()))-alreadyConnected.begin();
            alreadyConnected.erase(alreadyConnected.begin()+removeIndac);

        }
        firstclickflag = false;
        return true;
    }
    return false;
}
bool testApp::removeAnalogOutputConnect(string name)
{
    if(ofContains(analogOutputConnectionID,name))
    {
        int removeInd=find(analogOutputConnectionID.begin(), analogOutputConnectionID.end(),name)-analogOutputConnectionID.begin();
        analogOutputConnectionID.erase(analogOutputConnectionID.begin()+removeInd);
        analogOutputConnectionVars.erase(analogOutputConnectionVars.begin()+3*removeInd,analogOutputConnectionVars.begin()+3*removeInd+3);
        *analogOutputConditionVars[removeInd]=false;
        analogOutputConditionVars.erase(analogOutputConditionVars.begin()+removeInd);
        analogOutputConnectionCordis.erase(analogOutputConnectionCordis.begin()+2*removeInd,analogOutputConnectionCordis.begin()+2*removeInd+2);
        int removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),name.substr(name.find(","),name.length()))-alreadyConnected.begin();
        alreadyConnected.erase(alreadyConnected.begin()+removeIndac);

        string inName=analogInputConnectionID[removeInd];
        analogInputConnectionID.erase(analogInputConnectionID.begin()+removeInd);
        analogInputConnectionVars.erase(analogInputConnectionVars.begin()+3*removeInd,analogInputConnectionVars.begin()+3*removeInd+3);
        *analogInputConditionVars[removeInd]=false;
        analogInputConditionVars.erase(analogInputConditionVars.begin()+removeInd);
        analogInputConnectionCordis.erase(analogInputConnectionCordis.begin()+2*removeInd,analogInputConnectionCordis.begin()+2*removeInd+2);
        removeIndac=find(alreadyConnected.begin(), alreadyConnected.end(),inName)-alreadyConnected.begin();
        alreadyConnected.erase(alreadyConnected.begin()+removeIndac);
        return true;
    }
    return false;
}

void testApp::ldrDetach(int n)
{
    //int i = 1000;
    //while(i--)
    //cout<<"hello"<<n<<endl;
    for(int i=0; i<ldr[n]->Params.size(); i++){
        removeDigitalInputConnect(ldr[n]->Params[i].digitalConnectId);
        removeAnalogInputConnect(ldr[n]->Params[i].analogConnectId);
    }
    //delete ldr[n];
    //ldr[n] = new LDR(n);

    //ldr[n]->Params.erase(ldr[n]->Params.begin()+1,ldr[n]->Params.end());

}

void testApp::ptmDetach(int n)
{
    for(int i=0; i<ptm[n]->Params.size(); i++){
        removeDigitalInputConnect(ptm[n]->Params[i].digitalConnectId);
        removeAnalogInputConnect(ptm[n]->Params[i].analogConnectId);
    }
    //ptm[n]->Params.clear();
}

void testApp::ledDetach(int n)
{
    for(int i=0; i<led[n]->Params.size(); i++){
        removeDigitalOutputConnect(led[n]->Params[i].digitalConnectId);
        removeAnalogOutputConnect(led[n]->Params[i].analogConnectId);
    }
    //led[n]->Params.clear();
}

void testApp::buzDetach(int n)
{
    for(int i=0; i<buz[n]->Params.size(); i++){
        removeDigitalOutputConnect(buz[n]->Params[i].digitalConnectId);
        removeAnalogOutputConnect(buz[n]->Params[i].analogConnectId);
    }
   // buz[n]->Params.clear();
}

void testApp::sroDetach(int n)
{
    for(int i=0; i<sro[n]->Params.size(); i++){
        removeDigitalOutputConnect(sro[n]->Params[i].digitalConnectId);
        removeAnalogOutputConnect(sro[n]->Params[i].analogConnectId);
    }
    //sro[n]->Params.clear();
}

void testApp::kbdDetach(int n)
{
    for(int i=0; i<kbd->Params.size(); i++){
        removeDigitalOutputConnect(kbd->Params[i].digitalConnectId);
        //removeAnalogOutputConnect(kbd[n]->Params[i].analogConnectId);
    }
    //kbd[n]->Params.clear();
}

void testApp::exit()
{
    //gui->saveSettings("GUI/guiSettings.xml");
    //gui2->saveSettings("GUI/gui2Settings.xml");
    delete gui;
    //delete gui2;
    //serial.close();
}
//--------------------------------------------------------------
void testApp::keyPressed(int key)
{

         switch (key)
    {
        case 'p':
        {
            drawPadding = !drawPadding;
            gui->setDrawWidgetPadding(drawPadding);
        }
            break;

        case 'g':
        {
            gui->toggleVisible();
        }
            break;

        case 'f':
        {
            ofToggleFullscreen();
        }
            break;

        default:
            break;
    }
}
//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y )
{

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{


}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}
