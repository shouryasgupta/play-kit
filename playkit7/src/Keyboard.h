#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "ofMain.h"
#include "ofxUI.h"


class Keyboard
{
    public:

        //constructor
        Keyboard(int n);
        int id;
        string idstr;
        string type; //should we make type a static variable ??

        int attached();
        bool detached();
        void addParams();
        void removeParams();
        ofxUISuperCanvas *gui;

        bool addState;
        int lastON;
        bool lastONflag;

        typedef struct Param
        {
            bool keyflag;
            bool TrigORHold;
            float trigtime;
            int keytime;
            bool digitalValue;//OnOff;
            bool digitalConnect;//OnOffConnect;
            float digitalConnectx;
            float digitalConnecty;
            vector <string> keys;
            string digitalConnectId;
        } Param;

        //vector to store all the struct values
        vector  <Param> Params;







    protected:
    private:
};

#endif // KEYBOARD_H
