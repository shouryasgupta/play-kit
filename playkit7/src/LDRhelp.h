
#ifndef LDRHELP_H
#define LDRHELP_H
#include "ofMain.h"
#include "ofxUI.h"



class LDRhelp
{
    public:

        // constructor
        LDRhelp();
        string idstr;
        float xInit ;
        float length ;
        float dim ;


        void visibility();
        void invisible();
        void info();
        //void guiEvent(ofxUIEventArgs &e);
        ofxUISuperCanvas *gui;
        ofTrueTypeFont  ldr_info;




    protected:
    private:
};

#endif


