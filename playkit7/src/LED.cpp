#include "LED.h"

LED::LED(int n)
{
    // initialize the basic identifiers
    Params.reserve(100);
    id = n;
    idstr = "LED " + ofToString(id);
    type = "analogOut";

    lastONflag = false;
    //int xvar=5000;
    //while(xvar--)
    //cout<<"loloolololololololololololoooololo"<<endl;
    // create a new temp param struct, fill it with defaults wherever required and push it onto the vector...
    Param p;
    p.rangeMin = 0;
    p.rangeMax = 255; //dude this is LED !!!
    p.analogConnect = false; //dunno why this was getting set to true by default :p and that doesn't allow them to toggle as well!
    p.digitalConnect = false;
    p.digitalValue = false;
    Params.push_back(p);


    // create GUI - naming all elements as per params eg. Analog Value 0, 1, 2... for when you press the "+" button
    // because the NAME of the widgets must be unique inside a canvas ! -> turns out you can have multiple with same name!
    // look at the labels!, but you won't be able to refer to them later...in our case we only want to refer the pos of imgtoggles!
    // oh and also all the ones for widgetEastof !!! phew - mehnat safal :D
    int index = (Params.size() - 1);
    string indexstr = ofToString(index);
    string addrstr = "," + idstr + "," + indexstr;
    Params[0].digitalConnectId="Digital Output Connect"+addrstr;
    Params[0].analogConnectId="Analog Output Connect"+addrstr;
    gui = new ofxUISuperCanvas(idstr, 600,10+n*50, 200, 200, OFX_UI_FONT_MEDIUM);
    gui->addSpacer(210,2);
    //BAKCHODI! write the one that comes 1st, 1st! aur haan, label toh beech me hi hoga na duffer :p
    gui->addLabel("Brightness"+addrstr)->setLabel("Brightness");
    gui->addImageToggle("Analog Output Connect"+addrstr, "Preview.png", &Params[index].analogConnect)->setColorBack(ofColor(255,50));
    gui->addWidgetEastOf(gui->getWidget("Brightness"+addrstr), "Analog Output Connect"+addrstr,true); //this looks so much better !
    gui->addBiLabelSlider("Analog Value"+addrstr, "Dim", "Bright", 0, 255, &Params[index].analogValue);
    //gui->addWidgetSouthOf(gui->getWidget("Analog Value"+addrstr), "Analog Value"+addrstr,true);
    gui->addLabel("Set Range");
    gui->addRangeSlider("Set Range"+addrstr, 0, 255, &Params[index].rangeMin, &Params[index].rangeMax)->setLabelVisible(false);
    //gui->addImageButton("Dummy","Preview.png",false)->setVisible(false);
    //gui->addWidgetEastOf(gui->getWidget("Set Range"+addrstr),"Dummy",true);
    gui->addLabelToggle("On/Off"+addrstr, &Params[index].digitalValue,false)->setLabelText("On/Off");
    gui->addImageToggle("Digital Output Connect"+addrstr, "Preview.png", &Params[index].digitalConnect)->setColorBack(ofColor(255,50));
    //gui->addLabelToggle("Digital Output Connect"+addrstr,&Params[index].digitalConnect)->setLabelText("");
    gui->addWidgetEastOf(gui->getWidget("On/Off"+addrstr),"Digital Output Connect"+addrstr,true);
    gui->addSpacer(0,5);
    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+"); // the idstr helps in identifying whose + sign it is inside the event handler!
    gui->getWidget("Analog Value"+addrstr)->setDrawOutline(true);
    gui->getWidget("Set Range"+addrstr)->setDrawOutline(true);
    gui->getWidget("On/Off"+addrstr)->setDrawOutline(true);
    gui->getWidget("+" + idstr)->setDrawOutline(true);
    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(72,61,139));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);
    gui->setVisible(false);
}


void LED::addParams()
{
    // first create the param & then push it to the vector
    Param p;
    p.rangeMin = 0;
    p.rangeMax = 255; // dude this is LED !!!
    p.analogConnect = false; //dunno why these are getting set to true by default :p and that doesn't allow them to toggle as well!
    p.digitalConnect = false;
    p.digitalValue = false;
    Params.push_back(p);

    int index;
    string indexstr;
    string addrstr;

    //the normal adding doesn't work out, so we reset the whole UI !
    //first get the current position so that we can reset it at the same place...
    int x = gui->getRect()->x;
    int y = gui->getRect()->y;
    //then disable the old UI
    gui->disable();
    //reset it at the current position to hide this resetting from the user
    gui = new ofxUISuperCanvas(idstr, x, y, 200, 200, OFX_UI_FONT_MEDIUM);


    for(int i=0; i<Params.size(); i++)
    {
        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;
        Params[i].digitalConnectId="Digital Output Connect"+addrstr;
        Params[i].analogConnectId="Analog Output Connect"+addrstr;
        gui->addSpacer(210,2);
        gui->addLabel("Brightness"+addrstr)->setLabel("Brightness");
        gui->addImageToggle("Analog Output Connect"+addrstr, "Preview.png", &Params[index].analogConnect)->setColorBack(ofColor(255,50));
        gui->addWidgetEastOf(gui->getWidget("Brightness"+addrstr), "Analog Output Connect"+addrstr,true);
        gui->addBiLabelSlider("Analog Value"+addrstr, "Dim", "Bright", 0, 255, &Params[index].analogValue);
        gui->addLabel("Set Range");
        gui->addRangeSlider("Set Range"+addrstr, 0, 255, &Params[index].rangeMin, &Params[index].rangeMax)->setLabelVisible(false);
        gui->addLabelToggle("On/Off"+addrstr, &Params[index].digitalValue,false)->setLabelText("On/Off");
        gui->addImageToggle("Digital Output Connect"+addrstr, "Preview.png", &Params[index].digitalConnect)->setColorBack(ofColor(255,50));
        gui->addWidgetEastOf(gui->getWidget("On/Off"+addrstr),"Digital Output Connect"+addrstr,true);

        gui->getWidget("Analog Value"+addrstr)->setDrawOutline(true);
        gui->getWidget("Set Range"+addrstr)->setDrawOutline(true);
        gui->getWidget("On/Off"+addrstr)->setDrawOutline(true);

        gui->addSpacer(0,5);

    }

    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");
    gui->getWidget("+" + idstr)->setDrawOutline(true);

    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(72,61,139));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);

}


void LED::removeParams()
{
    if(Params.size()>1){
    Params.erase(Params.end()-1);
    int index;
    string indexstr;
    string addrstr;

    //the normal adding doesn't work out, so we reset the whole UI !
    //first get the current position so that we can reset it at the same place...
    int x = gui->getRect()->x;
    int y = gui->getRect()->y;
    //then disable the old UI
    gui->disable();
    //reset it at the current position to hide this resetting from the user
    gui = new ofxUISuperCanvas(idstr, x, y, 200, 200, OFX_UI_FONT_MEDIUM);


    for(int i=0; i<Params.size(); i++)
    {
        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;
        Params[i].digitalConnectId="Digital Output Connect"+addrstr;
        Params[i].analogConnectId="Analog Output Connect"+addrstr;
        gui->addSpacer(210,2);
        gui->addLabel("Brightness"+addrstr)->setLabel("Brightness");
        gui->addImageToggle("Analog Output Connect"+addrstr, "Preview.png", &Params[index].analogConnect)->setColorBack(ofColor(255,50));
        gui->addWidgetEastOf(gui->getWidget("Brightness"+addrstr), "Analog Output Connect"+addrstr,true);
        gui->addBiLabelSlider("Analog Value"+addrstr, "Dim", "Bright", 0, 255, &Params[index].analogValue);
        gui->addLabel("Set Range");
        gui->addRangeSlider("Set Range"+addrstr, 0, 255, &Params[index].rangeMin, &Params[index].rangeMax)->setLabelVisible(false);
        gui->addLabelToggle("On/Off"+addrstr, &Params[index].digitalValue,false)->setLabelText("On/Off");
        gui->addImageToggle("Digital Output Connect"+addrstr, "Preview.png", &Params[index].digitalConnect)->setColorBack(ofColor(255,50));
        gui->addWidgetEastOf(gui->getWidget("On/Off"+addrstr),"Digital Output Connect"+addrstr,true);

        gui->getWidget("Analog Value"+addrstr)->setDrawOutline(true);
        gui->getWidget("Set Range"+addrstr)->setDrawOutline(true);
        gui->getWidget("On/Off"+addrstr)->setDrawOutline(true);

        gui->addSpacer(0,5);

    }

    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");
    gui->getWidget("+" + idstr)->setDrawOutline(true);

    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(72,61,139));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);
    }
}


int LED::attached()
{

    int index;
    string indexstr;
    string addrstr;
   // cout<<"attachedattachedattachedattachedattachedattachedattachedattachedattachedattachedattached"<<endl;
    // make the gui visible first
    gui->setVisible(true);

    // setting the intrinsic properties and returning apt results to outValue backend variable
    for(int i=0; i<Params.size(); i++)
    {
        // value can only be within the range
        if( Params[i].analogValue < Params[i].rangeMin )
            Params[i].analogValue = Params[i].rangeMin;

        if( Params[i].analogValue > Params[i].rangeMax )
            Params[i].analogValue = Params[i].rangeMax;

        // only one set can be ON at a time, so either choose true logic, else the system will allow only one to be ON

        // check which one is on, set the flag high and also store the id. useful later to supply outValue and also to block other ONs
        if( Params[i].digitalValue == true && lastONflag == false)
        {
            lastON = i;
            lastONflag = true;
        }

        // only if the one which was ON is turned OFF, will the flag be cleared and will allow new ONs
        if( Params[i].digitalValue == false && i == lastON && lastONflag == true )
        {
            lastONflag =false;
        }

        // don't allow any other turn ONs
        if( Params[i].digitalValue == true && i!= lastON && lastONflag == true)
        {
            Params[i].digitalValue = false;
        }


        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;

        //calculate the co-ordis
        Params[i].digitalConnectx = gui->getRect()->x + gui->getWidget("Digital Output Connect"+addrstr)->getRect()->x; //output dev- gui cordi + widget cordi !
        Params[i].digitalConnecty = gui->getRect()->y + gui->getWidget("Digital Output Connect"+addrstr)->getRect()->y + gui->getWidget("Digital Output Connect"+addrstr)->getRect()->height/2;
        Params[i].analogConnectx = gui->getRect()->x + gui->getWidget("Analog Output Connect"+addrstr)->getRect()->x;
        Params[i].analogConnecty = gui->getRect()->y + gui->getWidget("Analog Output Connect"+addrstr)->getRect()->y + gui->getWidget("Analog Output Connect"+addrstr)->getRect()->height/2;

    }

    // return the value of the active one. if none is active, means all are off, so return 0 as per the intrinsic property !
    if( lastONflag == true)
        return Params[lastON].analogValue;
    else
        return 0;


}

bool LED::detached()
{
    //while(Params.size()>1)
    //removeParams();
    bool flag=false;
    if(Params.size()>1)
    flag=true;
    while(Params.size()>1)
    removeParams();
    gui->setVisible(false);
    //gui->setVisible(false);
    return flag;
}
