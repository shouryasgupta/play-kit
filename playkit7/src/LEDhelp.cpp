
#include "LEDhelp.h"

LEDhelp::LEDhelp()
{
        string idstr= "LED";
        xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
        length = 255-xInit;
        dim =20;
        info1 =0;


        //ledbtn=false;
     gui= new ofxUISuperCanvas(idstr, 220, 220, 100, 120, OFX_UI_FONT_MEDIUM);
      // gui->setColorOutlineHighlight(ofColor (0,0,0));
     //gui->addSpacer((length-xInit)/2, 2);
    // gui->setTheme(OFX_UI_THEME_MACOSX);
     //gui->addWidgetDown(new ofxUILabel("Emits Light ", OFX_UI_FONT_MEDIUM));
     ofSetColor(255,255,255);
     gui->addWidgetDown(new ofxUIImageButton(dim*4.0, dim*4.0, true, "whiteLED.jpg","LEDBTN"));
    //gui->addImageToggle("LEDBTN", "whiteLED.jpg", &ledbtn,dim*4.0, dim*4.0)->setColorBack(ofColor(255,50));
       gui->setDrawWidgetPadding(true);

     gui->setColorBack(ofColor(72,61,139));
     gui->setDrawOutline(true);
     gui->setDrawOutlineHighLight(true);
      gui->setColorBack(ofColor(72,61,139));
//     ofAddListener(gui->newGUIEvent,this,&LEDhelp::guiEvent);
     gui->setVisible(false);
      led_info.loadFont("cooperBlack.ttf",15);
      led_info.setLineHeight(18.0f);
	  led_info.setLetterSpacing(1.037);
	  gui->autoSizeToFitWidgets();

}

void LEDhelp::visibility()
{
    gui->setVisible(true);
}
void LEDhelp::invisible()
{
    gui->setVisible(false);
}



