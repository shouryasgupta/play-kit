#include "Ptm.h"

Ptm::Ptm(int n)
{
    // initialize the basic identifiers
    Params.reserve(100);
    id = n;
    //idstr = "Light Sensor " + ofToString(id);
    idstr = "potentiometer " + ofToString(id);
    type = "analogIn";

    // create a new temp param struct, fill it with defaults wherever required and push it onto the vector...
    Param p;
    p.rangeMin = 0;
    p.rangeMax = 1023;
    p.analogConnect = false; //dunno why these are getting set to true by default :p
    p.digitalConnect = false;
    Params.push_back(p);


    // create GUI - naming all elements as per params eg. Analog Value 0, 1, 2... for when you press the "+" button
    // because the NAME of the widgets must be unique inside a canvas ! -> turns out you can have multiple with same name!
    // look at the labels!, but you won't be able to refer to them later...in our case we only want to refer the pos of imgtoggles!
    // oh and also all the ones for widgetEastof !!! phew - mehnat safal :D
    int index = (Params.size() - 1);
    string indexstr = ofToString(index);
    string addrstr = "," + idstr + "," + indexstr; // complete address for reference! used everywhere now :p
    Params[0].digitalConnectId="Digital Input Connect"+addrstr;
    Params[0].analogConnectId="Analog Input Connect"+addrstr;
    gui = new ofxUISuperCanvas(idstr,10+n*50,400, 200, 200, OFX_UI_FONT_MEDIUM);
    gui->addSpacer(200,2);
    gui->addImageToggle("Analog Input Connect"+addrstr, "Preview.png", &Params[index].analogConnect)->setColorBack(ofColor(255,50));
    //gui->addLabel("Brightness");
    //ofSetColor(112,138,144);
    gui->addLabel("Value");
    //gui->addBiLabelSlider("Analog Value"+addrstr, "Dark", "Bright", 0, 1023, &Params[index].analogValue);
    gui->addBiLabelSlider("Analog Value"+addrstr, "Low", "High", 0, 1023, &Params[index].analogValue);
    gui->addWidgetEastOf(gui->getWidget("Analog Input Connect"+addrstr), "Analog Value"+addrstr,true);
    gui->addLabel("Set Range");    gui->addRangeSlider("Set Range"+addrstr, 0, 1023, &Params[index].rangeMin, &Params[index].rangeMax)->setLabelVisible(false);
    gui->addImageToggle("Digital Input Connect"+addrstr, "Preview.png", &Params[index].digitalConnect)->setColorBack(ofColor(255,50));
    gui->addLabelToggle("Range In/Out"+addrstr, &Params[index].digitalValue,false)->setLabelText("Range In/Out");
    gui->addWidgetEastOf(gui->getWidget("Digital Input Connect"+addrstr),"Range In/Out"+addrstr,true);
    gui->addSpacer(0,5);
    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");

    gui->getWidget("Analog Value"+addrstr)->setDrawOutline(true);
    gui->getWidget("Set Range"+addrstr)->setDrawOutline(true);
    gui->getWidget("Range In/Out"+addrstr)->setDrawOutline(true);
    gui->getWidget("+" + idstr)->setDrawOutline(true);
    gui->autoSizeToFitWidgets();

    gui->setColorBack(ofColor(60,60,255));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);

    gui->setVisible(false);


}

void Ptm::addParams()
{
    // first create the param & then push it to the vector
    Param p;
    p.rangeMin = 0;
    p.rangeMax = 1023;
    p.analogConnect = false; //dunno why these are getting set to true by default :p
    p.digitalConnect = false;
    Params.push_back(p);

    int index;
    string indexstr;
    string addrstr;

    //the normal adding doesn't work out, so we reset the whole UI !
    //first get the current position so that we can reset it at the same place...
    int x = gui->getRect()->x;
    int y = gui->getRect()->y;
    //then disable the old UI
    gui->disable();
    //reset it at the current position to hide this resetting from the user
    gui = new ofxUISuperCanvas(idstr, x, y, 200, 200, OFX_UI_FONT_MEDIUM);


    for(int i=0; i<Params.size(); i++)
    {
        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;
        Params[i].digitalConnectId="Digital Input Connect"+addrstr;
        Params[i].analogConnectId="Analog Input Connect"+addrstr;
        gui->addSpacer(210,2);
        gui->addImageToggle("Analog Input Connect"+addrstr, "Preview.png", &Params[index].analogConnect)->setColorBack(ofColor(255,50));
        //gui->addLabel("Brightness");
        gui->addLabel("Value");
        //gui->addBiLabelSlider("Analog Value"+addrstr, "Dark", "Bright", 0, 1023, &Params[index].analogValue);
        gui->addBiLabelSlider("Analog Value"+addrstr, "Low", "High", 0, 1023, &Params[index].analogValue);
        gui->addWidgetEastOf(gui->getWidget("Analog Input Connect"+addrstr), "Analog Value"+addrstr,true);
        gui->addLabel("Set Range");
        gui->addRangeSlider("Set Range"+addrstr, 0, 1023, &Params[index].rangeMin, &Params[index].rangeMax)->setLabelVisible(false);
        gui->addImageToggle("Digital Input Connect"+addrstr, "Preview.png", &Params[index].digitalConnect)->setColorBack(ofColor(255,50));
        gui->addLabelToggle("Range In/Out"+addrstr, &Params[index].digitalValue,false)->setLabelText("Range In/Out");
        gui->addWidgetEastOf(gui->getWidget("Digital Input Connect"+addrstr),"Range In/Out"+addrstr,true);

        gui->getWidget("Analog Value"+addrstr)->setDrawOutline(true);
        gui->getWidget("Set Range"+addrstr)->setDrawOutline(true);
        gui->getWidget("Range In/Out"+addrstr)->setDrawOutline(true);

        gui->addSpacer(0,5);
    }

    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");
    gui->getWidget("+" + idstr)->setDrawOutline(true);

    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(60,60,255));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);

}
void Ptm::removeParams()
{
    if(Params.size()>1){
    Params.erase(Params.end()-1);
    int index;
    string indexstr;
    string addrstr;

    //the normal adding doesn't work out, so we reset the whole UI !
    //first get the current position so that we can reset it at the same place...
    int x = gui->getRect()->x;
    int y = gui->getRect()->y;
    //then disable the old UI
    gui->disable();
    //reset it at the current position to hide this resetting from the user
    gui = new ofxUISuperCanvas(idstr, x, y, 200, 200, OFX_UI_FONT_MEDIUM);


    for(int i=0; i<Params.size(); i++)
    {
        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;
        Params[i].digitalConnectId="Digital Input Connect"+addrstr;
        Params[i].analogConnectId="Analog Input Connect"+addrstr;
        gui->addSpacer(210,2);
        gui->addImageToggle("Analog Input Connect"+addrstr, "Preview.png", &Params[index].analogConnect)->setColorBack(ofColor(255,50));
        //gui->addLabel("Brightness");
        gui->addLabel("Value");
        //gui->addBiLabelSlider("Analog Value"+addrstr, "Dark", "Bright", 0, 1023, &Params[index].analogValue);
        gui->addBiLabelSlider("Analog Value"+addrstr, "Low", "High", 0, 1023, &Params[index].analogValue);
        gui->addWidgetEastOf(gui->getWidget("Analog Input Connect"+addrstr), "Analog Value"+addrstr,true);
        gui->addLabel("Set Range");
        gui->addRangeSlider("Set Range"+addrstr, 0, 1023, &Params[index].rangeMin, &Params[index].rangeMax)->setLabelVisible(false);
        gui->addImageToggle("Digital Input Connect"+addrstr, "Preview.png", &Params[index].digitalConnect)->setColorBack(ofColor(255,50));
        gui->addLabelToggle("Range In/Out"+addrstr, &Params[index].digitalValue,false)->setLabelText("Range In/Out");
        gui->addWidgetEastOf(gui->getWidget("Digital Input Connect"+addrstr),"Range In/Out"+addrstr,true);

        gui->getWidget("Analog Value"+addrstr)->setDrawOutline(true);
        gui->getWidget("Set Range"+addrstr)->setDrawOutline(true);
        gui->getWidget("Range In/Out"+addrstr)->setDrawOutline(true);

        gui->addSpacer(0,5);
    }

    addState = false;
    gui->addLabelToggle("+" + idstr,&addState)->setLabelText("+");
    gui->getWidget("+" + idstr)->setDrawOutline(true);

    gui->autoSizeToFitWidgets();
    gui->setColorBack(ofColor(60,60,255));
    gui->setDrawOutline(true);
    gui->setDrawOutlineHighLight(true);
    }
}
void Ptm::attached(int inValue)
{

    int index;
    string indexstr;
    string addrstr;

    //make the gui visible
    gui->setVisible(true);

    //set the input values for all the params, also set the intrinsic properties
    for(int i=0; i<Params.size(); i++)
    {
        Params[i].analogValue = inValue;

        if( Params[i].analogValue >= Params[i].rangeMin && Params[i].analogValue <= Params[i].rangeMax)
            Params[i].digitalValue = true;
        else
            Params[i].digitalValue = false;


        index = i;
        indexstr = ofToString(index);
        addrstr = "," + idstr + "," + indexstr;

        //calculate the co-ordis
        //cout<<Params[i].digitalValue<<endl;
        //if(Params[i].digitalValue==true)
        //    gui->setColorBack(ofColor(120,180));
       // else
        //    gui->setColorBack(ofColor(0,180));
        Params[i].digitalConnectx = gui->getRect()->x + gui->getWidget("Digital Input Connect"+addrstr)->getRect()->x + gui->getWidget("Digital Input Connect"+addrstr)->getRect()->width; //input dev- gui cordi + widget cordi + width!
        Params[i].digitalConnecty = gui->getRect()->y + gui->getWidget("Digital Input Connect"+addrstr)->getRect()->y + gui->getWidget("Digital Input Connect"+addrstr)->getRect()->height/2;
        Params[i].analogConnectx = gui->getRect()->x + gui->getWidget("Analog Input Connect"+addrstr)->getRect()->x + gui->getWidget("Analog Input Connect"+addrstr)->getRect()->width;
        Params[i].analogConnecty = gui->getRect()->y + gui->getWidget("Analog Input Connect"+addrstr)->getRect()->y + gui->getWidget("Analog Input Connect"+addrstr)->getRect()->height/2;
    }

}

bool Ptm::detached()
{
     bool flag=false;
    if(Params.size()>1)
    flag=true;
    while(Params.size()>1)
    removeParams();
    gui->setVisible(false);
    return flag;
}
