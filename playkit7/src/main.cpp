#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"

//========================================================================
int main( ){

    ofAppGlutWindow window;
//	window.setGlutDisplayString("rgba double samples>=6 depth");
	ofSetupOpenGL(&window, 1280, 720, OF_WINDOW);
	//ofSetupOpenGL(&window, 1330, 690, OF_WINDOW);			// <-------- setup the GL context
	//ofSetupOpenGL(&window, 	ofGetScreenWidth(), ofGetScreenHeight(), OF_WINDOW);			// <-------- setup the GL context
	//ofSetupOpenGL();
//	window->initializeWindow();
	ofRunApp( new testApp());
}
