#ifndef LEDHELP_H
#define LEDHELP_H
#include "ofMain.h"
#include "ofxUI.h"

class LEDhelp
{
    public:

        // constructor
        LEDhelp();
        string idstr;
        float xInit ;
        float length ;
        float dim ;


        void visibility();
        void invisible();
        void info();
        void draw();
        //void guiEvent(ofxUIEventArgs &e);
        ofxUISuperCanvas *gui;
        ofTrueTypeFont  led_info;
        int info1;





    protected:
    private:
};

#endif
