
#ifndef BUZZERHELP_H
#define BUZZERHELP_H
#include "ofMain.h"
#include "ofxUI.h"



class BUZZERhelp
{
    public:

        // constructor
        BUZZERhelp();
        string idstr;
        float xInit ;
        float length ;
        float dim ;


        void visibility();
        void invisible();
        void info();
        //void guiEvent(ofxUIEventArgs &e);
        ofxUISuperCanvas *gui;
        ofTrueTypeFont  buzzer_info;




    protected:
    private:
};

#endif



