#ifndef MIC_H
#define MIC_H

#include "ofMain.h"
#include "ofxUI.h"
#include<vector>
#include <string>

class Ptm
{
    public:

        // constructor
        Ptm(int n);
        int id;
        string idstr;
        string type;

        // the attached method does the connection with backend variables and applies intrinsic properties
        // basically all things needed to be done while connected
        // detached disables the viewing etc stuff...
        void attached(int inValue); //need to supply this value, problem accessing testApp, hierarchy locha. Return for outValue
        bool detached();

        // to add a new set of params when "+" is clicked
        void addParams();
        void removeParams();
        // the main UI
        ofxUISuperCanvas *gui;
        bool addState;

        // a struct to make it easier to access values and to push it onto a vector later on !
        typedef struct Param
        {
            float analogValue;
            float rangeMin;
            float rangeMax;
            bool analogConnect;
            bool digitalValue;//rangeInOut;
            bool digitalConnect;//rangeInOutConnect;
            float digitalConnectx;
            float digitalConnecty;
            float analogConnectx;
            float analogConnecty;
            string digitalConnectId;
            string analogConnectId;
        } Param;

        //vector to store all the struct values
        std::vector<Param> Params;


    protected:
    private:
};

#endif // MIC_H
