


#ifndef POTHELP_H
#define POTHELP_H
#include "ofMain.h"
#include "ofxUI.h"



class POThelp
{
    public:

        // constructor
        POThelp();
        string idstr;
        float xInit ;
        float length ;
        float dim ;


        void visibility();
        void invisible();
        void info();
        //void guiEvent(ofxUIEventArgs &e);
        ofxUISuperCanvas *gui;
        ofTrueTypeFont  pot_info;




    protected:
    private:
};

#endif

