#ifndef BUZZER_H
#define BUZZER_H

#include "ofMain.h"
#include "ofxUI.h"

class Buzzer
{
    public:

        // constructor
        Buzzer(int n);
        int id;
        string idstr;
        string type; //should we make type a static variable ??

        // the attached method does the connection with backend variables and applies intrinsic properties
        // basically all things needed to be done while connected
        // detached disables the viewing etc stuff...
        int attached(); //Returns the outValue, not messing with pointers as of now...
        bool detached();

        // to add a new set of params when "+" is clicked
        void addParams();
        void removeParams();
        // the main UI
        ofxUISuperCanvas *gui;

        // duh :p
        bool addState;

        // last to be ON
        int lastON;
        bool lastONflag;

        // a struct to make it easier to access values and to push it onto a vector later on !
        typedef struct Param
        {
            float analogValue;
            float rangeMin;
            float rangeMax;
            bool analogConnect;
            bool digitalValue;//OnOff;
            bool digitalConnect;//OnOffConnect;
            float digitalConnectx;
            float digitalConnecty;
            float analogConnectx;
            float analogConnecty;
            string digitalConnectId;
            string analogConnectId;
        } Param;

        //vector to store all the struct values
        vector  <Param> Params;




    protected:
    private:
};

#endif // BUZZER_H
