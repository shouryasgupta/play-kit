
#include "BUZZERhelp.h"

BUZZERhelp::BUZZERhelp()
{
        string idstr= "BUZZER";
        xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
        length = 255-xInit;
        dim =20;
       gui= new ofxUISuperCanvas(idstr, 220, 360, 100, 120, OFX_UI_FONT_MEDIUM);
         gui->setColorOutlineHighlight(ofColor (0,0,0));
        gui->addSpacer(length-xInit, 2);
       // gui5->addWidgetDown(new ofxUILabel("Light controlled res", OFX_UI_FONT_MEDIUM));
        gui->addWidgetDown(new ofxUIImageButton(dim*4.0, dim*4.0, true, "BUZZER1.jpg","BUZZERBTN"));
          gui->setDrawWidgetPadding(true);
//        ofAddListener(gui->newGUIEvent,this,&testApp::guiEvent);
     gui->setColorBack(ofColor(153,0,153));
     gui->setDrawOutline(true);
     gui->setDrawOutlineHighLight(true);
     //gui->autoSizeToFitWidgets();
//     ofAddListener(gui->newGUIEvent,this,&LEDhelp::guiEvent);
     gui->setVisible(false);
      buzzer_info.loadFont("cooperBlack.ttf",15);
      buzzer_info.setLineHeight(18.0f);
	  buzzer_info.setLetterSpacing(1.037);
	  //gui->autoSizeToFitWidgets();
}

void BUZZERhelp::visibility()
{
    gui->setVisible(true);
}
void BUZZERhelp::invisible()
{
    gui->setVisible(false);
}
void BUZZERhelp::info()
{
  buzzer_info.drawString("Makes Buzzy sound !",400,650);

}




