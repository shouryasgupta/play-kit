
#include "LDRhelp.h"

LDRhelp::LDRhelp()
{
        string idstr= "LDR";
        xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
        length = 255-xInit;
        dim =20;
        ofSetColor(0,0,0);
       gui= new ofxUISuperCanvas(idstr, 220, 80, 100, 120, OFX_UI_FONT_MEDIUM);
        gui->setColorOutlineHighlight(ofColor (0,0,0));
        gui->addSpacer(length-xInit, 2);
       // gui5->addWidgetDown(new ofxUILabel("Light controlled res", OFX_UI_FONT_MEDIUM));
        gui->addWidgetDown(new ofxUIImageButton(dim*4.0, dim*4.0, true, "LDR.jpg","LDRBTN"));
     gui->setDrawWidgetPadding(true);

     gui->setDrawOutline(true);
     gui->setDrawOutlineHighLight(true);
      gui->setColorBack(ofColor(34,139,34));
//     ofAddListener(gui->newGUIEvent,this,&LEDhelp::guiEvent);
     gui->setVisible(false);
      ldr_info.loadFont("cooperBlack.ttf",15);
      ldr_info.setLineHeight(18.0f);
	  ldr_info.setLetterSpacing(1.037);
	  //gui->autoSizeToFitWidgets();
}

void LDRhelp::visibility()
{
    gui->setVisible(true);
}
void LDRhelp::invisible()
{
    gui->setVisible(false);
}
void LDRhelp::info()
{
  ldr_info.drawString("Light controlled resistor !",400,350);

}
/*void LEDhelp::guiEvent(ofxUIEventArgs &e)
{
    string name = e.widget->getName();
	int kind = e.widget->getKind();
	if(name == "LEDBTN"){
	 ofxUIImageButton *button = (ofxUIImageButton *) e.widget;
     cout << "value: " << button->getValue() << endl;
	}

}*/


