#ifndef TESTAPP
#define TESTAPP

#include "ofMain.h"
#include "ofxUI.h"

#include "LDR.h"
#include "Ptm.h"
#include "LED.h"
#include "Keyboard.h"
#include "Buzzer.h"
#include "Servo.h"
#include "LEDhelp.h"
#include "LDRhelp.h"
#include "BUZZERhelp.h"
#include "Buzzerinfo.h"
#include "LEDinfo.h"
#include "LDRinfo.h"
#include "POThelp.h"

class testApp : public ofBaseApp
{
	public:
	void setup();
	void update();
	void draw();
	void exit();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

    void setupGUIs();
	void setupMenu();
	void guiEvent(ofxUIEventArgs &e);
    ofxUISuperCanvas *guiMessage;

    void updateArduino();
    bool removeDigitalInputConnect(string name);
    bool removeDigitalOutputConnect(string name);
    bool removeAnalogInputConnect(string name);
    bool removeAnalogOutputConnect(string name);

    void ldrDetach(int n);
    void ptmDetach(int n);
    void ledDetach(int n);
    void buzDetach(int n);
    void sroDetach(int n);
    void kbdDetach(int n);

    LDR* ldr[4];
    Ptm *ptm[4];
    LED *led[4];
    Keyboard *kbd;
    Buzzer *buz[4];
    Servo *sro[4];

   /* LDR *ldr2;
    Ptm *ptm2;
    LED *led2;
    Keyboard *kbd2;
    Buzzer *buz2;
    Servo *sro2;

    LDR *ldr3;
    Ptm *ptm3;
    LED *led3;
    Keyboard *kbd3;
    Buzzer *buz3;
    Servo *sro3;

    LDR *ldr4;
    Ptm *ptm4;
    LED *led4;
    Keyboard *kbd4;
    Buzzer *buz4;
    Servo *sro4;*/

    int clickFlagA;
    int clickFlagM;
    int clickFlagB;
    int clickFlagK;
    int clickFlagL;
    int clickFlagS;
    int clickFlagT;

    bool kbdIsConnected;

    bool connected;
    bool halfconnected;
    bool firstclickflag;
    bool secondclickflag;
    vector <string> firstclickdata;
    vector <string> secondclickdata;


    vector <bool*> digitalInputConnectionVars;
    vector <bool*> digitalOutputConnectionVars;
    vector <bool*> digitalInputConditionVars;
    vector <bool*> digitalOutputConditionVars;

    vector <float*> analogInputConnectionVars;
    vector <float*> analogOutputConnectionVars;
    vector <bool*> analogInputConditionVars;
    vector <bool*> analogOutputConditionVars;

    vector <string> alreadyConnected;

    vector <float*> digitalInputConnectionCordis;
    vector <float*> digitalOutputConnectionCordis;
    vector <float*> analogInputConnectionCordis;
    vector <float*> analogOutputConnectionCordis;

    vector <string> digitalInputConnectionID;
    vector <string> digitalOutputConnectionID;
    vector <string> analogInputConnectionID;
    vector <string> analogOutputConnectionID;

    // v0.1 uses ldr(ain), led(aout), sw(din), buzz & servo
    bool connected_ldr_led_digital;
    bool connected_ldr_led_analog;
    bool connected_ldr_buzzer_digital;
    bool connected_ldr_buzzer_analog;
    bool connected_ldr_servo_digital;
    bool connected_ldr_servo_analog;
    bool connected_ldr_keyboard_digital; // KB is digi out so can connect only in a digital fashion!

    bool connected_switch_led_digital;
    bool connected_switch_buzzer_digital;
    bool connected_switch_servo_digital;
    bool connected_switch_keyboard_digital;

//    bool connected_pot_led_digital;
//    bool connected_pot_led_analog;
//    bool connected_pot_buzzer_digital;
//    bool connected_pot_buzzer_analog;

    ofSerial        serial;
    bool		    bSendSerialMessage;			// a flag for sending serial
    char		    bytesReadString[19];			// a string needs a null terminator, so we need 6 + 1 bytes
    int 			nBytesRead;					// how much did we read?
    float	    	readTime;					// when did we last read?
    bool            msgack;


    unsigned char   lsbi,lsbo,msbi,msbo;
    vector<unsigned char> inputDevice;
    vector<unsigned char> outputDevice;
    int cycleNumber;
    int inValue;
    vector<int> outValue;

    float lineInX;
    float lineInY;
    float lineOutX;
    float lineOutY;


    int red;
    int green;
    int blue;
    int resolution;

    int r; //range

    float testvar;
    int digi; // if clicked just on input device then to check it was analog(digi=0)/digital(digi=1)
    vector<int> wire_togg; // if digital connection is active it is used to toggle color of wire

	// starting menu
		ofxUICanvas *gui;
       // void guiEvent(ofxUIEventArgs &e);
        bool drawPadding;
	    //float red, green, blue;
	    string typeStr;
	    int chk1;
	    int chk2;
	    int chk3;
	    int help;

		ofTrueTypeFont  title;
		ofTrueTypeFont msg_help;
		ofImage trans;
         float xInit ;
        float length ;
        int bg;

  //   ofTrueTypeFont testFont;
        LEDhelp *led1;
        LDRhelp *ldr1;
       LDRinfo *ldrin1;
        LEDinfo *ledin1;
        BUZZERhelp *buzz1;
        Buzzerinfo *buzzin1;
       POThelp *pot1;
        ofImage tit;
        int tempvar;

};

#endif
